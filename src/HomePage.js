

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

class HomePage extends React.Component {

  render() {
    return (
      <div>
        <h2><Link to="/QuickStart">00_QuickStart</Link></h2>
        <h2><Link to="/AdvancedGuides">01_AdvancedGuides</Link></h2>
        <h2><Link to="/Redux">02_Redux</Link></h2>
        <h2><Link to="/Components">Components</Link></h2>
      </div>
    );
  }
}

export default HomePage;
