

import HomePage from './HomePage';

import QuickStart from './00_QuickStart'
import AdvancedGuides from './01_AdvancedGuides'
import Redux from './02_Redux'
import Components from './99_ComponentsTests'

import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';//tudo ue tiver dentro dele será atualizado com o hot-loader
import { BrowserRouter, Route, Link } from "react-router-dom";

import './filesImport'

function renderApp(){
  ReactDOM.render(
    <AppContainer>
      <div>
        <BrowserRouter>
        <div>
          <Route exact path='/' component={HomePage} />

          <Route path='/QuickStart' component={QuickStart} />
          <Route path='/AdvancedGuides' component={AdvancedGuides} />
          <Route path='/Redux' component={Redux} />
          <Route path='/Components' component={Components} />
        </div>
        </BrowserRouter>
      </div>
    </AppContainer>,
    document.querySelector('[data-js="app"]')
  );
};

let isFirst = true
if(module.hot || isFirst){//apenas em modo de desenvolvimento
    renderApp()
    isFirst = false
};
