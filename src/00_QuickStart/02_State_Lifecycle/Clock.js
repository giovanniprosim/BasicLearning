

import React from 'react';
import ReactDOM from 'react-dom';

class Clock extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      header:"Hello, world!",
      date: new Date(),
    };

  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillMount(){
    //sem usar os props
    this.setState({header: "Olá, mundo!"});
    //com props...arrow function
    this.setState((prevState, props) => ({
      header: prevState.header + " " + props.user
    }));
    //com props... regular function
    this.setState(function(prevState, props) {
      return {
        header: prevState.header + " " + props.user
      }
    })
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        <h1>{this.state.header}</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }

}

export default Clock;
