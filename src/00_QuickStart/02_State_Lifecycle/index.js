

import Clock from './Clock';

import React from 'react';
import ReactDOM from 'react-dom';

class index extends React.Component {

  render() {
    return (
        <div>
          <Clock key="c1" user="Giovanni" />
          <Clock key="c2" user="Raquel" />
          <Clock key="c3" user="Tobias" />
        </div>
    );
  }
}

export default index;
