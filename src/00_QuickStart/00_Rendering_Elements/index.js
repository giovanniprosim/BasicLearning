

import React from 'react';
import ReactDOM from 'react-dom';

class index extends React.Component {

  componentWillMount(){
    this.element = (
      <div>
        <h1>Hello, world!</h1>
        <h2>{new Date().toLocaleTimeString()}</h2>
      </div>
    );
  }

  render() {
    return (
        <div>
          {this.element}
        </div>
    );
  }
}

export default index;
