

import Toggle from './Toggle';

import React from 'react';
import ReactDOM from 'react-dom';

class index extends React.Component {

  activateLasers(e) {
    e.preventDefault();
    console.log('The link was clicked.', e, e.type, e.target);
  }

  render() {
    var self = this;
    return (
        <div>
          <button href="#" onClick={self.activateLasers}>
            Activate Lasers
          </button>
          <Toggle />
        </div>
    );
  }
}

export default index;
