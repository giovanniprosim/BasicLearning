

import React from 'react';
import ReactDOM from 'react-dom';

class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = (id, e) => {
    console.log("arguments... ", arguments,"\n", e,"\n", id);
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }

  render() {

    var style = {
      color: "white",
      backgroundColor: this.state.isToggleOn ?  "green" : "red"
    };

    return (
      <div>
        <button style={style} onClick={this.handleClick.bind(this, 0)}>
          {this.state.isToggleOn ? 'ON' : 'OFF'}
        </button>
        {
          !this.state.isToggleOn &&
          <h2>Por favor, aperte no botão para liga-lo...</h2>
        }
      </div>
    );
  }
}

export default Toggle;
