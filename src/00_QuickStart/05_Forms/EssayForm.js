

import React from 'react';
import ReactDOM from 'react-dom';

class EssayForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'Write anything'
    };

    this.handleChange = this.handleChange.bind(this);
    this.props.onTextAreaChange(this.state.value);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
    this.props.onTextAreaChange(event.target.value);
  }

  get Value(){
    return this.state.value;
  }

  render() {
    return (
      <div>
        <label>
          Obs:
          <textarea value={this.state.value} onChange={this.handleChange} />
        </label>
      </div>
    );
  }
}

export default EssayForm;
