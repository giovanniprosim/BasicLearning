import EssayForm from './EssayForm';
import FlavorForm from './FlavorForm';
import Reservation from './Reservation';

import React from 'react';
import ReactDOM from 'react-dom';

class index extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    console.log("handleChange...",value);
    this.setState({value: event.target.value.toUpperCase()});
  }

  FormChange(value){
    console.log("FormChange...",value);
  }

  FlavorChange(value){
    console.log("FlavorChange...",value);
  }

  ReservationChange(obj_value){
    console.log("ReservationChange...", obj_value);
  }

  handleSubmit(event) {
    alert(
      'A name was submitted: ' + this.state.value
      + "\nobs: " + this.form.Value
      + "\nchoice: " + this.flavor.Selected
    );
    event.preventDefault();
  }

  render() {
    return (
        <div>
          <form onSubmit={this.handleSubmit}>
            <div>
              <label>
                Name:
                <input type="text" value={this.state.value} onChange={this.handleChange} />
              </label>
            </div>
            <div>
              <EssayForm onTextAreaChange={this.FormChange} />
            </div>
            <div>
              <FlavorForm onSelectedChange={this.FlavorChange} />
            </div>
            <div>
              <Reservation onReservationChange={this.ReservationChange} />
            </div>
            <div>
              <input type="submit" value="Submit" />
            </div>
          </form>
        </div>
    );
  }
}

export default index;
