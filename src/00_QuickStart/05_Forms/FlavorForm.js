

import React from 'react';
import ReactDOM from 'react-dom';

class FlavorForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: 'coconut'};

    this.handleChange = this.handleChange.bind(this);
    this.props.onSelectedChange(this.state.value);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
    this.props.onSelectedChange(event.target.value);
  }

  render() {
    return (
      <div>
        <label>
          Pick your favorite La Croix flavor:
          <select value={this.state.value} onChange={this.handleChange}>
            <option value="grapefruit">Grapefruit</option>
            <option value="lime">Lime</option>
            <option value="coconut">Coconut</option>
            <option value="mango">Mango</option>
          </select>
        </label>
      </div>
    );
  }
}

export default FlavorForm
