

import React from 'react';
import ReactDOM from 'react-dom';

class NumberList extends React.Component {

  constructor(props) {
    super(props);

    var numbers = props.numbers;
    const listItems = numbers.map((item, index, array) => {
      var key = "nl_" + index;
      return <li key={key}>{item}</li>
    });
    this.htmlList = <ul>Number List:{listItems}</ul>

  }

  render() {

    return (
      <div>
        {this.htmlList}
      </div>

    );

  }
}

export default NumberList;
