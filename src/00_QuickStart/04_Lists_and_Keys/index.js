

import NumberList from './NumberList';
import Index from './teste/index';

import React from 'react';
import ReactDOM from 'react-dom';

class index extends React.Component {

  constructor(props){
    super(props);

    this.numbers = [1,2,3,4,5];
  }

  componentWillMount(){
    this.listItems = this.numbers.map((item, index, array) => {
      var key="n"+index;
      return <li key={key}>{index} : Estou passando pelo número {item}... {array}</li>
    });
  }

  render() {
    return (
        <div>
          <ul>{this.listItems}</ul>

          <NumberList numbers={this.numbers}/>

          <ul>Lista Embedada...
          {this.numbers.map((item, index, array) => {
              var key="nEmb_"+index;
              return <li key={key}>{index} : Embedando o {item}... {array}</li>
            })
          }
        </ul>
        </div>
    );
  }
}

export default index;
