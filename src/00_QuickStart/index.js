

import Home from './Home';
import page00 from './00_Rendering_Elements/index';
import page01 from './01_Components_and_Props/index';
import page02 from './02_State_Lifecycle/index';
import page03 from './03_Handling_Events/index';
import page04 from './04_Lists_and_Keys/index';
import page05 from './05_Forms/index';
import page06 from './06_Composition_X_Inheritance/index';

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

class index extends React.Component {

  render() {
    return (
      <div>
        <BrowserRouter>
        <div>
          <Route exact path='/QuickStart/' component={Home} />
          <Route path='/QuickStart/Rendering_Elements' component={page00} />
          <Route path='/QuickStart/Components_and_Props' component={page01} />
          <Route path='/QuickStart/State_Lifecycle' component={page02} />
          <Route path='/QuickStart/Handling_Events' component={page03} />
          <Route path='/QuickStart/Lists_and_Keys' component={page04} />
          <Route path='/QuickStart/Forms' component={page05} />
          <Route path='/QuickStart/Composition_X_Inheritance' component={page06} />
        </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default index;
