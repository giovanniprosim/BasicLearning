

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

class Home extends React.Component {

  render() {
    return (
      <div>
        <h2><Link to="/QuickStart/Rendering_Elements">00_Rendering_Elements</Link></h2>
        <h2><Link to="/QuickStart/Components_and_Props">01_Components_and_Props</Link></h2>
        <h2><Link to="/QuickStart/State_Lifecycle">02_State_Lifecycle</Link></h2>
        <h2><Link to="/QuickStart/Handling_Events">03_Handling_Events</Link></h2>
        <h2><Link to="/QuickStart/Lists_and_Keys">04_Lists_and_Keys</Link></h2>
        <h2><Link to="/QuickStart/Forms">05_Forms</Link></h2>
        <h2><Link to="/QuickStart/Composition_X_Inheritance">06_Composition_X_Inheritance</Link></h2>
      </div>
    );
  }
}

export default Home;
