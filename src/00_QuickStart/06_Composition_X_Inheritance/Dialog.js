

import React from 'react';
import ReactDOM from 'react-dom';

import FancyBorder from './FancyBorder'

import Style from './css/style'

class Dialog extends React.Component {

  render() {
    return (
      <FancyBorder color={this.props.color}>
        <h1 className="Dialog-title">
          {this.props.title}
        </h1>
        <p className="Dialog-message">
          {this.props.message}
        </p>
        {this.props.children}
      </FancyBorder>
    );
  }
}

export default Dialog;
