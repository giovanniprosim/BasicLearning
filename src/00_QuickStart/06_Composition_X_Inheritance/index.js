

import React from 'react';
import ReactDOM from 'react-dom';

import FancyBorder from './FancyBorder'
import Dialog from './Dialog'

class index extends React.Component {

  componentWillMount(){
    this.dialog = {
      title: "Welcome",
      message: "Thank you for visiting our spacecraft!",
      color: "blue",
      children: (<div>I'm your children!</div>)
    }
  }

  render() {
    return (
        <div>
          <FancyBorder color="green">
            Testando o styled-components
          </FancyBorder>

          <Dialog {...this.dialog} />
        </div>
    );
  }
}

export default index;
