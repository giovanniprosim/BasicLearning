import styled from 'styled-components'

const style = styled.div`

.FancyBorder {
  padding: 10px 10px;
  border: 10px solid;
}

.FancyBorder-${props => props.color} {
  border-color: ${props => props.color};
}

.Dialog-title {
  margin: 0;
  font-family: sans-serif;
}

.Dialog-message {
  font-size: larger;
}

`

export default style
