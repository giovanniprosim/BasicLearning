

import React from 'react';
import ReactDOM from 'react-dom';

import Style from './css/style'

class FancyBorder extends React.Component {

  constructor(props){
    super(props)
  }

  componentWillMount(){

  }

  render() {
    return (
      <Style color={this.props.color}>
        <div className={'FancyBorder FancyBorder-' + this.props.color} >
          {this.props.children}
        </div>
      </Style>
    );
  }
}

export default FancyBorder;
