

import Welcome from './Welcome';
import Comment from "./Comment";

import React from 'react';
import ReactDOM from 'react-dom';

class index extends React.Component {

  componentWillMount(){
    this.element = <this.Hello name="Sara" />;

    this.comment = {
      date: (new Date()).toUTCString(),
      text: 'I hope you enjoy learning React!',
      author: {
        name: 'Hello Kitty',
        avatarUrl: 'http://placekitten.com/g/64/64'
      }
    };
  }

  Hello(props) {
    return <h1>Hello, {props.name}</h1>;
  }

  AnotherNames(args){

    var list_names = args.names;
    var html = [];

    for (var i = 0; i < list_names.length; i++) {
      var c_name = list_names[i];
      var id = "welcome_" + i;
      html[i] = <Welcome key={id} name={c_name} />;
    }

    return(
      <div>{html}</div>
    )
  }

  render() {
    return (
        <div>
          {this.element}
            <Welcome name="João" />
            <this.AnotherNames names={["Anastacio", "Manuel", "Jaqueline"]}/>

            <Comment
              date={this.comment.date}
              text={this.comment.text}
              author={this.comment.author} />
        </div>
    );
  }
}

export default index;
