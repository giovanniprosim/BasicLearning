

import React from 'react';
import ReactDOM from 'react-dom';

import Avatar from './Avatar';
import UserInfo from './UserInfo';

class Comment extends React.Component {
  render() {
    return (
      <div className="Comment">
        <UserInfo user={this.props.author} />
        <div className="Comment-text">
          {this.props.text}
        </div>
        <div className="Comment-date">
          {this.props.date}
        </div>
      </div>
    );
  }
}

export default Comment;
