

import React from 'react';
import ReactDOM from 'react-dom';

class Avatar extends React.Component {
  render() {
    return (
      <img className="Avatar"
        src={this.props.user.avatarUrl}
        alt={this.props.user.name}
      />
    );
  }
}

export default Avatar;
