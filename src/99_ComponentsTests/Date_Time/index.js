

import React from 'react';
import ReactDOM from 'react-dom';

import DateTime from '../../API/Components/Styled/Datetime'

class index extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount(){

  }

  dtOnChange() {
    console.log("Change the date");
  }

  render() {
    return (
        <div>
          <p>Simple</p>
          <DateTime />

          <p>Configured</p>
          <DateTime
            onChange={this.dtOnChange}
            disableOnClickOutside={true}
            closeOnSelect={true}
            closeOnTab={true}
            open={false}
            dateFormat="YYYY-MM-DD"
            timeFormat={false}
            inputProps={
              {
                placeholder: "Clique aqui para inserir uma data..."
              }
            }
          />

        </div>
    );
  }
}

export default index;
