import React from 'react';
import ReactDOM from 'react-dom';

import { Grid, Row, Col } from 'react-flexbox-grid'

class index extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount(){

  }

  render() {
    return (
        <div>
          <h2>React-Flexbox-Grid</h2>
          <Grid fluid>
            <Row>
              <Col xs={12} sm={5} md={4} lg={4} className="col-button-loadMore">
                <div>Div 1</div>
              </Col>
              <Col xs={12} sm={5} md={4} lg={4} className="col-button-loadMore">
                <div>Div 2</div>
              </Col>
              <Col xs={12} sm={5} md={4} lg={4} className="col-button-loadMore">
                <div>Div 3</div>
              </Col>
            </Row>
          </Grid>

        </div>
    );
  }
}

export default index;
