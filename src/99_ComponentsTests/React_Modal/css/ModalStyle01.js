import styled from 'styled-components'

const ModalStyle01 = styled.div`

  .backdropStyle {
    position: fixed;
    top: 0px;
    bottom: 0px;
    left: 0px;
    right: 0px;
    background-color: rgba(0,0,0,0.3);
    padding: 50px;
  }

  .modalStyle {
    background-color: #fff;
    border-radius: 5px;
    max-width: 500px;
    min-height: 300px;
    margin: 0 auto;
    padding: 30px;
  }

`

export default ModalStyle01
