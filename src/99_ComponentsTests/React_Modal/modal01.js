

import React from 'react';
import ReactDOM from 'react-dom';

import ModalStyle01 from './css/ModalStyle01'

/***

Modal reference:
https://daveceddia.com/open-modal-in-react/
https://github.com/dceddia/modal-in-react/blob/master/src/Modal.js

***/

class Modal01 extends React.Component {
  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    return (
      <ModalStyle01>
        <div id="modalBack" className="backdropStyle"
          onClick={(e)=>{
            if(e.target != document.getElementById('modal')) {
                this.props.onClose()
            }
          }}>
          <div id="modal" className="modalStyle">
            {this.props.children}

            <div className="footer">
              <button onClick={this.props.onClose}>
                Close
              </button>
            </div>
          </div>
        </div>
      </ModalStyle01>
    );
  }
}

export default Modal01;
