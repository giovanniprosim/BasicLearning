

import React from 'react';
import ReactDOM from 'react-dom';

import Modal01 from './modal01';
import ReactModal from 'react-modal'
/** reference: https://reactcommunity.org/react-modal/ **/

class index extends React.Component {

  constructor(props) {
    super(props);

    this.state = { isOpen: false };
  }

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  toggleReactModal = () => {
    this.setState({
      isOpenReactModal: !this.state.isOpenReactModal
    });
  }

  toggleReactModal2 = () => {
    this.setState({
      isOpenReactModal2: !this.state.isOpenReactModal2
    });
  }

  componentWillMount(){

  }

  render() {
    return (
        <div>
          <button onClick={this.toggleModal}>
            Modal01
          </button>

          <Modal01 show={this.state.isOpen}
            onClose={this.toggleModal}>
            Here's some content for the modal
          </Modal01>



          <button onClick={this.toggleReactModal}>
            ReactModal Centralizado
          </button>

          <ReactModal
            isOpen={this.state.isOpenReactModal}
            shouldCloseOnOverlayClick={true}//if close on click outside
            onRequestClose={this.toggleReactModal}
            closeTimeoutMS={500}//time wait to close
            style={{
             overlay: {
               backgroundColor: 'rgba(1,1,1,0.5)',
              //alinhar centralizado horizontal e verticalmente
               display: 'flex',
               alignItems: 'center',
               justifyContent: 'center',
             },
             content: {
               backgroundColor: 'white',
               color: 'black',
               top: 'auto',
               left: 'auto',
               right: 'auto',
               bottom: 'auto',
               padding: '50px'
             }
           }}
          >
            Estou dentro do Modal do react, pode me chamar de pop-up se quiser...
          </ReactModal>



          <button onClick={this.toggleReactModal2}>
            ReactModal Minimo
          </button>

          <ReactModal
            isOpen={this.state.isOpenReactModal2}
            shouldCloseOnOverlayClick={true}//if close on click outside
            onRequestClose={this.toggleReactModal2}
          >
            Esse é um modal comum usando o mínimo necessário para ser feliz
          </ReactModal>
        </div>

    );
  }
}

export default index;
