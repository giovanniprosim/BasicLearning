

import React from 'react';
import ReactDOM from 'react-dom';

import SimpleCollapse from '../../API/Components/Base/Collapse/SimpleCollapse'
/** reference: https://reactcommunity.org/react-modal/ **/

class index extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount(){

  }

  render() {
    return (
        <div>

          <SimpleCollapse
            header={<div>Main Header</div>}
            content={<div>Content</div>}
          />

          <br />

          <SimpleCollapse
            header={<div>Main Header</div>}
            content={
              <div>
                <SimpleCollapse
                  header={<div>Header 1</div>}
                  content={<div>Content 1</div>}
                />
                <SimpleCollapse
                  header={<div>Header 2</div>}
                  content={<div>Content 2</div>}
                />
              </div>
            }
          />

        </div>
    );
  }
}

export default index;
