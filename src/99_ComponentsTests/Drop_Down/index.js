

import React from 'react';
import ReactDOM from 'react-dom';

import DropDown from '../../API/Components/Styled/Dropdown'

class index extends React.Component {

  componentWillMount(){

  }

  selectValue(item) {
    console.log("Item choiced is...", item);
  }

  render() {
    return (
        <div>
          <DropDown
            selectedLabel="Selecione um item..."
            selectedValue=""
            items={
              (
                [
                  {value:'0', label:"Teste 1"},
                  {value:'1', label:"Teste 2"},
                  {value:'2', label:"Teste 3"},
                  {value:'3', label:"Teste 4"},
                ]
              )
            }
            onClick={this.selectValue}
          />
        </div>
    );
  }
}

export default index;
