

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

class Home extends React.Component {

  render() {
    return (
      <div>
        <h2><Link to="/Components/FlexBoxGrid">FlexBoxGrid</Link></h2>
        <h2><Link to="/Components/Collapse">Collapse</Link></h2>
        <h2><Link to="/Components/Date_Time">DateTime</Link></h2>
        <h2><Link to="/Components/Drop_Down">DropDown</Link></h2>
        <h2><Link to="/Components/React_Modals">React_Modals</Link></h2>
        <h2><Link to="/Components/TweenMax">TweenMax</Link></h2>
      </div>
    );
  }
}

export default Home;
