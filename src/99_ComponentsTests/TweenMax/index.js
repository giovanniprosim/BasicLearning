

import React from 'react';
import ReactDOM from 'react-dom';

import TweenMax from 'gsap'
// https://greensock.com/docs/
// https://greensock.com/docs/Easing

class index extends React.Component {

  constructor(props) {
    super(props);

    this.TimeLinePlay = false
    this.Reversed = false

    this.toggleAnimation = this.toggleAnimation.bind(this)
    this.reverseAnimation = this.reverseAnimation.bind(this)
  }

  componentWillMount(){

    this.divs = [
      (<div id="left_rigth" style={this.divStyle('red')}>LEFT TO RIGTH</div>),
      (<div id="rigth_left" style={this.divStyle('blue')}>RIGTH TO LEFT</div>),
      (<div id="grow" style={this.divStyle('green')}>GROW UP</div>),
      (<div id="decrease" style={this.divStyle('yellow')}>DECREASE</div>),
    ]

  }

  componentDidMount(){

    this.allDivsRefs = [
      this.getDivByID('left_rigth'),
      this.getDivByID('rigth_left'),
      this.getDivByID('grow'),
      this.getDivByID('decrease'),
    ]

    //ANIMATION 1
    // TweenMax.fromTo(
    //   this.allDivsRefs[1], 2,
    //   {right: 0, opacity: 0},
    //   {right:200, opacity: 1, ease: Bounce.easeOut}
    // )

    //ANIMATION 2
    // TweenMax.staggerTo(this.allDivsRefs, 2, {left:"+=200"}, 1)

    //ANIMATION 3
    this.tl = new TimelineMax({
      onComplete: () => {
        console.log("Completei toda a animação!")
        this.refs.playBtn.setAttribute('disabled', true)
        this.refs.reverseBtn.setAttribute('disabled', true)
        this.toggleAnimation(false)
      /*this.tl.kill()*/
    },
      onCompleteScope: this,
    })
    this.tl.staggerTo(this.allDivsRefs, 0, {opacity: 0}, 0)
    this.tl.to(this.allDivsRefs[0], 1, {opacity: 1, left:"+=200", ease: Elastic.easeOut});
    this.tl.fromTo(this.allDivsRefs[1], 2,
      {right: 0, opacity: 0},
      {right:200, opacity: 1, ease: Bounce.easeOut},
    )
    this.tl.fromTo(this.allDivsRefs[2], 3,
      {left:200, top:200, scale: 0, opacity: 0},
      {opacity: 1, scale: 2, ease: Bounce.easeIn},
    )
    this.tl.fromTo(this.allDivsRefs[3], 3,
      {left:400, top:400, scale: 3, opacity: 1},
      {scale: 0.5 , ease: Elastic.easeOut},
    )
    this.toggleAnimation(true)

  }

  getDivByID (id){
    return this.refs.mainDiv.querySelector('#'+id)
  }

  divStyle(color) {
    return {backgroundColor:color, position:'absolute', width: 'auto'}
  }

  toggleAnimation(force) {
    this.TimeLinePlay = force || !this.TimeLinePlay
    if(this.TimeLinePlay){
      this.refs.playBtn.innerHTML = "Stop"
      if(this.tl.time() > 0){this.tl.resume()}
      else{this.tl.play()}
    }
    else{
      this.refs.playBtn.innerHTML = "Play"
      this.tl.pause()
    }
  }

  reverseAnimation(){
    this.Reversed = !this.Reversed
    this.tl.reversed( this.Reversed );
    this.refs.reverseBtn.innerHTML = this.Reversed ? "Normal" : "Reverse"
  }

  render() {
    return (
        <div ref="mainDiv">

          {this.divs}

          <br /><br />
          <button ref="playBtn" onClick={()=>this.toggleAnimation()} ></button>
          <button ref="reverseBtn" onClick={()=>this.reverseAnimation()} >Reverse</button>

        </div>
    );
  }
}

export default index;
