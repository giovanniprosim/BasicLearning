

import Home from './Home';

import modal from './React_Modal/index';
import collapse from './Collapse/index';
import tweenMax from './TweenMax/index';

import flexBoxGrid from './FlexBoxGrid/index';
import dropdown from './Drop_Down/index';
import datetime from './Date_Time/index';

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

class index extends React.Component {

  render() {
    return (
      <div>
        <BrowserRouter>
        <div>
          <Route exact path='/Components/' component={Home} />

          <Route path='/Components/Collapse' component={collapse} />
          <Route path='/Components/Date_Time' component={datetime} />
          <Route path='/Components/Drop_Down' component={dropdown} />
          <Route path='/Components/FlexBoxGrid' component={flexBoxGrid} />
          <Route path='/Components/React_Modals' component={modal} />
          <Route path='/Components/TweenMax' component={tweenMax} />
        </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default index;
