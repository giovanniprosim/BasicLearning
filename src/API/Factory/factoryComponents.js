import React, { Component } from 'react'
import PropTypes from 'prop-types'

//1 API
//1.1 Base
import SimpleCollapse from 'API_ComponentsBase/Collapse/SimpleCollapse'
import ReactModal from 'API_ComponentsBase/Modal/ReactModal'

//1.2 Styled
import DateTime from 'API_ComponentsStyled/Datetime'
import DropDown from 'API_ComponentsStyled/Dropdown'

//2 Packages
import { Grid, Row, Col } from 'react-flexbox-grid'

class FactoryComponents extends Component {

  constructor(props) {
    super(props)

    this.components = {
      //Base
      SimpleCollapse,
      ReactModal,

      //Styled
      DateTime,
      DropDown,

      //Packages
      Grid, Row, Col
    }
  }

  //TODO quando tiver animações será usada essa função
  // componentDidMount() {
  // }

  createComponent(componentName, config, childrens) {

    const FactoryComponent =  this.components[componentName] || componentName

    return (
      <FactoryComponent {...config}>
        {
          (config && config.children) ||
          (
            childrens &&
            childrens.map(child => {
              const {componentName, config, childrens} = child
              return this.createComponent(componentName, config, childrens)
            })
          )
        }
      </FactoryComponent>
    )
  }

  render() {
    const {componentName, config, childrens} = this.props

    return (
      this.createComponent(componentName, config, childrens)
    )
  }
}

//TODO pode ser que todos os components venham do redux
// function mapStateToProps({ cars }) {
//   const { carsList, carDetail, carsQtt, isLoading } = cars
//
//   return {
//     carsList,
//     carDetail,
//     carsQtt,
//     isLoading,
//   }
// }
//
// function mapDispatchToProps(dispatch) {
//   return {
//     actions: bindActionCreators({
//       ...CarsActions,
//     }, dispatch),
//   }
// }

export default FactoryComponents
