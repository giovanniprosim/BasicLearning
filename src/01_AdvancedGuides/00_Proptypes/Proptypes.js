

import React from 'react';
import ReactDOM from 'react-dom';

import PropTypes from 'prop-types'
import Message from './Message'

class Proptypes extends React.Component {

  constructor(props){
    super(props)
  }

  render() {
    console.log("this.props",this.props)
    return (
        <div>
          <div>{this.props.name}</div>
          <div>{this.props.age}</div>
          {
            this.props.contactNumbers.map((item, id) => {
              return <div key={id}>{item}</div>
            })
          }
          <div>{this.props.optionalMessage}</div>
          <div>{this.props.optionalFunction}</div>
          <div>{this.props.obs}</div>
          <div>{this.props.reactElement}</div>
        </div>
    );
  }
}

Proptypes.propTypes = {
  props: PropTypes.shape({
    name: PropTypes.string.isRequired,
    age: PropTypes.number.isRequired,
    isSingle: PropTypes.bool,
    contactNumbers: PropTypes.arrayOf(PropTypes.string),
    optionalMessage: PropTypes.instanceOf(Message),
    optionalFunction: PropTypes.func,
    obs: 'Without observation...',
    reactElement: PropTypes.element,
  })
}

export default Proptypes;
