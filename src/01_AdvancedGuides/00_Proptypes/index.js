

import React from 'react';
import ReactDOM from 'react-dom';

import Proptypes from './Proptypes';
import Message from './Message';

class index extends React.Component {

  render() {
    return (
        <div>
          <Proptypes
            name={"Giovanni Pregnolato Rosim"}
            age={25}
            isSingle={true}
            contactNumbers={["995593220", "33351207"]}
            optionalMessage={<Message />}
          />
        </div>
    );
  }
}

export default index
