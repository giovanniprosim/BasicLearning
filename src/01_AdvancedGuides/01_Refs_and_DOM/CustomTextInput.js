

import React from 'react';
import ReactDOM from 'react-dom';

class CustomTextInput extends React.Component {

  constructor(props) {
    super(props)
    this.focusTextInput = this.focusTextInput.bind(this)
  }

  componentDidMount() {
    if(this.props.initWithFocus)
    this.focusTextInput()
  }

  focusTextInput() {
    // Explicitly focus the text input using the raw DOM API
    console.log("this.textInput", this.textInput);
    this.textInput.focus();
  }

  render() {
    return (
        <div>
          <input
            placeholder="anything..."
            type="text"
            ref={(input) => { this.textInput = input; }} />
          <input
            type="button"
            value="Focus the text input"
            onClick={this.focusTextInput}
          />
        </div>
    );
  }
}

export default CustomTextInput
