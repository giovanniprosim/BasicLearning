

import React from 'react';
import ReactDOM from 'react-dom';

import CustomTextInput from './CustomTextInput'

const Parent = ({ inputRef }) => (
    <div>
      Grandparent <Grandparent inputRef={inputRef}/>
    </div>
)

const Grandparent = ({ inputRef }) => (
  <div ref={inputRef}>
    Oi
  </div>
)

class index extends React.Component {

  constructor(props){
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    this.refs.div.textContent = "Input any text..."

    this.divElement.style.backgroundColor = "green"
  }

  handleSubmit(event) {
    console.log('A name was submitted: ', this.txtInput.value);
  }

  render() {
    return (
        <div>

          <div ref="div" />

          <CustomTextInput />
          <CustomTextInput initWithFocus={true}/>

          <Parent inputRef={el => this.divElement = el} />

          <label>
            Name:
            <input defaultValue="Ex: João" type="text" ref={(input) => this.txtInput = input} />
          </label>
          <button onClick={this.handleSubmit}>Submit</button>


        </div>
    );
  }
}

export default index
