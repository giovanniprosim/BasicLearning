

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

class Home extends React.Component {

  render() {
    return (
      <div>
        <h2><Link to="/AdvancedGuides/PropTypes">00_PropTypes</Link></h2>
        <h2><Link to="/AdvancedGuides/Refs_and_DOM">01_Refs_and_DOM</Link></h2>
      </div>
    );
  }
}

export default Home;
