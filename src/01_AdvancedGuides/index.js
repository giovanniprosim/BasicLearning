

import Home from './Home';
import page00 from './00_Proptypes/index';
import page01 from './01_Refs_and_DOM/index';

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

class index extends React.Component {

  render() {
    return (
      <div>
        <BrowserRouter>
        <div>
          <Route exact path='/AdvancedGuides/' component={Home} />
          <Route path='/AdvancedGuides/PropTypes' component={page00} />
          <Route path='/AdvancedGuides/Refs_and_DOM' component={page01} />
        </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default index;
