

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

class Home extends React.Component {

  render() {
    return (
      <div>
        <h2><Link to="/Redux/Basic">00_Basic</Link></h2>
        <h2><Link to="/Redux/Advanced">01_Advanced</Link></h2>
      </div>
    );
  }
}

export default Home;
