

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

//REDUX
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {actions} from '../Reducer/PersonReducer'

class index extends React.Component {

  componentWillMount() {
    this.props.actions.getPerson()
  }

  render() {

    const person = this.props.personReducer.person
    const personReducer = this.props.personReducer

    return (
      <div>
        <div style={{backgroundColor:'red'}}>{personReducer.errorMsg}</div>
        <div style={{backgroundColor:'green'}}>{personReducer.loading ? "Carregando..." : null}</div>
        <div>Language: {this.props.current}</div>
        <img src={person.photoBin} />
        {/* <img src={person.photoSrc} /> */}
        <div>Name: {person.name}</div>
        <div>Birth Day: {person.birthDay}</div>
        <div>Sex: {person.sex}</div>
        <div>Height: {person.height}</div>
        <div>Weight: {person.weight}</div>
        <br />
        <div>{personReducer.txt}</div>
      </div>
    );
  }
}

function mapStateToProps( {personReducer, languagesReducer} ) {

  const { current, languages } = languagesReducer

  return {
    personReducer,
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...actions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(index)
