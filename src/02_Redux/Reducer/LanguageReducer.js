const initialState = {
  current:'enUS',
  languages: {
    enUS:0,
    ptBR:1,
    esES:2,
    jaJA:3,
    frFR:4,
  },
}
const types = {
  SET_CURRENT: 'SET_CURRENT',
}

export const actions = {
  setCurrentLanguage: (language) => (dispatch) => {
    dispatch({
      type: types.SET_CURRENT,
      payload: language,
    })
  },
}

export default (state = initialState, { type, payload }) => {

  switch (type) {

    case types.SET_CURRENT:
      return {
        ...state,
        current: payload,
      }

    default:
      return state
  }
}
