import { combineReducers } from 'redux'

import languagesReducer from './LanguageReducer'
import personReducer from './PersonReducer'

export default combineReducers({
  languagesReducer,
  personReducer,
})
