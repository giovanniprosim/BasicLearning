const initialState = {
  loading:false,
  errorMsg: null,
  person: {},
  txt: "",
}

const types = {
  ON_LOADING: 'ON_LOADING',
  PERSON_FAIL: 'PERSON_FAIL',
  GET_PERSON: 'GET_PERSON',

  TXT_FAIL: 'TXT_FAIL',
  GET_TXT: 'GET_TXT',
}

import person from '../01_Advanced/files/person.json'
import txt from '../01_Advanced/files/test.txt'// /bin/files/test.txt

export const actions = {
  getPerson: () => (dispatch) => {

    dispatch({
        type: types.GET_PERSON,
        payload: {...person}
    })

    dispatch({
        type: types.ON_LOADING,
        payload: {}
    })

    var request = new XMLHttpRequest();
    request.open('GET', txt, true);
    request.send()

    request.onload = () => {
      if (request.status >= 200 && request.status < 400) {
        dispatch({
          type: types.GET_TXT,
          payload: request.responseText
        })
      } else {
        dispatch({
          type: types.TXT_FAIL,
          payload: "We reached our target server, but it returned an error",
        })
      }
    };

    request.onerror = () => {
      dispatch({
        type: types.TXT_FAIL,
        payload: "There was a connection error of some sort!!!",
      })
    }

  },

}

export default (state = initialState, { type, payload }) => {

  switch (type) {

    case types.GET_PERSON:
      return {
        ...state,
        person: payload,
        loading: false,
      }

    case types.PERSON_FAIL:
      return {
        ...state,
        person: {},
        loading: false,
        errorMsg: payload,
      }

    case types.GET_TXT:
      return {
        ...state,
        txt: payload,
        loading: false,
      }

    case types.TXT_FAIL:
      return {
        ...state,
        person: {},
        loading: false,
        errorMsg: payload,
      }

    case types.ON_LOADING:
      return {
        ...state,
        loading: true,
      }

    default:
      return state
  }
}
