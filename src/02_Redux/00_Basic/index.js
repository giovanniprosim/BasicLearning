

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

//REDUX
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {actions} from '../Reducer/LanguageReducer'

//CLASSES
import ChangeName from './ChangeName'

class index extends React.Component {

  render() {

    console.log("Renderizando index do REDUX BASIC");

    const enunLanguages = this.props.languages
    const languagesKeys = Object.keys(enunLanguages)

    const currentLanguage = this.props.current

    return (
      <div>
        {
          <div>
            Current Language =>
            {enunLanguages[currentLanguage]} :
            {currentLanguage}
          </div>
        }
        {languagesKeys.map((item, id) => {
            return (
              <div key={"lgg" + item}>
                {enunLanguages[item]} : {item}
              </div>
            )
          })
        }
        <ChangeName />
      </div>
    );
  }
}

function mapStateToProps( {languagesReducer} ) {
  const { current, languages } = languagesReducer
  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...actions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(index)
