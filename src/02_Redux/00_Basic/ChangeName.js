

import React from 'react';
import ReactDOM from 'react-dom';

//REDUX
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {actions} from '../Reducer/LanguageReducer'

class ChangeName extends React.Component {

  constructor(props) {
    super(props)

    this.changeLanguageRadioBtn = this.changeLanguageRadioBtn.bind(this)

  }

  changeLanguageRadioBtn() {
    const currentLanguage = this.refs.selectLgg.querySelector("input[name=language]:checked").value
    this.props.actions.setCurrentLanguage(currentLanguage)
  }

  render() {

    console.log("Renderizando o ChangeName.js");

    const allLanguages = this.props.languages
    const currentLanguage = this.props.current

    return (
      <div ref="selectLgg">
        { (
            () => {
              const lggKeys = Object.keys(allLanguages)
              return lggKeys.map((lgg) => {
                const isCurrent = currentLanguage === lgg
                const id = "lgg".concat(lgg)
                return (
                  <div key={lgg}>
                   <input
                      id={id}
                      type="radio"
                      name="language"
                      value={lgg}
                      onChange={this.changeLanguageRadioBtn}
                      defaultChecked={isCurrent}
                    />
                    <label htmlFor={id}>{lgg}</label>
                  </div>
                )
              })
            }
          )()
        }
      </div>
    )

  }
}

function mapStateToProps( {languagesReducer} ) {
  const { current, languages } = languagesReducer
  return {
    current,
    languages,
  }
}

function mapDispatchToProps(dispatch) {

  return {
    actions: bindActionCreators({
      ...actions,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeName)
