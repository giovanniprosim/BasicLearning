

import Home from './Home';
import basic from './00_Basic/index';
import advanced from './01_Advanced/index';
// import recipes from './02_Recipes/index';

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";

import AppReducer from './Reducer'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';

let store = createStore(
  AppReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk),
)
console.log("store...", store.getState());

class index extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
        <div>
          <Route exact path='/Redux/' component={Home} />
          <Route path='/Redux/Basic' component={basic} />
          <Route path='/Redux/Advanced' component={advanced} />
          {/* <Route path='/Redux/Recipes' component={recipes} /> */}
        </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default index;
